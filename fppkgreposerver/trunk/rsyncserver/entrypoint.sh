#!/bin/bash

__create_rundir() {
	mkdir -p /var/run/sshd
}

__create_hostkeys() {
ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key -N ''
}

# Call all functions
__create_rundir
# Do not re-create the hostkey on every new pod, but use the one
# configured in Kubernetes
#__create_hostkeys

exec "$@"
