FROM registry.fedoraproject.org/fedora-minimal

RUN microdnf -y update
RUN microdnf -y install openssl-devel
RUN microdnf -y clean all

RUN useradd --create-home --shell /bin/bash locuser